package com.n26.exception;

public class TransactionOutOfFutureWindow extends Exception {

    public TransactionOutOfFutureWindow(String s) {
        super(s);
    }
}
