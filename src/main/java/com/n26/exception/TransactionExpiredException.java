package com.n26.exception;

public class TransactionExpiredException extends Exception {


    public TransactionExpiredException(String s) {
        super(s);
    }
}
