package com.n26.api;

import com.n26.api.exception.handler.ErrorResponse;
import com.n26.api.swagger.GroupedSwaggerApiResponses;
import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;
import com.n26.json.TransactionPostJson;
import com.n26.service.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(consumes = "application/json;charset=UTF-8")
    @ApiOperation(
            value = "Saves the given transaction.",
            nickname = "save tx. ",
            response = Transaction.class
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "<response_json>"),
            @ApiResponse(code = 406, message = "Transaction has expired. Given timestamp is older than 1 min."),
            @ApiResponse(code = 422,
                    message = "Transaction out of window. Given timestamp is in future time."),
            @ApiResponse(
                    code = 400,
                    message = "bad request",
                    response = ErrorResponse.class),
            @ApiResponse(
                    code = 500,
                    message = "internal service exception",
                    response = ErrorResponse.class)
    })
    public ResponseEntity<Transaction> post(@RequestBody @Valid TransactionPostJson
                                                    bodyJson) throws TransactionOutOfFutureWindow, TransactionExpiredException {
        Transaction transaction = this.transactionService.process(bodyJson);

        return new ResponseEntity<>(transaction, HttpStatus.CREATED);
    }

    @DeleteMapping
    @ApiOperation(
            value = "Deletes All transaction.",
            nickname = "delete tx. "
    )
    @GroupedSwaggerApiResponses
    public ResponseEntity<Object> delete() {
        transactionService.delete();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
