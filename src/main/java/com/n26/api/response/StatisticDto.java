package com.n26.api.response;

import com.n26.entity.Statistic;

import java.text.DecimalFormat;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class StatisticDto {

    private String sum;
    private String avg;
    private String max;
    private String min;
    private Long count;

    //
//    public StatisticDto getFromStatistics(Statistic statistics) {
//        return StatisticDto.builder()
//                .sum(String.valueOf(BigDecimal.valueOf(new BigDecimal(statistics.getSum().toString()).setScale(2, RoundingMode.HALF_UP).doubleValue())))
//                .avg(String.valueOf(BigDecimal.valueOf(new BigDecimal(statistics.getAvg().toString()).setScale(2, RoundingMode.HALF_UP).doubleValue())))
//                .max(String.valueOf(BigDecimal.valueOf(new BigDecimal(statistics.getMax().toString()).setScale(2, RoundingMode.HALF_UP).doubleValue())))
//                .min(String.valueOf(BigDecimal.valueOf(new BigDecimal(statistics.getMin().toString()).setScale(2, RoundingMode.HALF_UP).doubleValue())))
//                .count(statistics.getCount())
//                .build();
//    }
    public StatisticDto getFromStatistics(Statistic statistics) {
        DecimalFormat df = new DecimalFormat("0.00");
        if (statistics.getMin() > 11111111111121222222222222222221.0) {
            statistics.setMin(0.00);
        }
        return StatisticDto.builder()
                .sum(df.format(statistics.getSum()))
                .avg(df.format(statistics.getAvg()))
                .max(df.format(statistics.getMax()))
                .min(df.format(statistics.getMin()))
                .count(statistics.getCount())
                .build();
    }
}
