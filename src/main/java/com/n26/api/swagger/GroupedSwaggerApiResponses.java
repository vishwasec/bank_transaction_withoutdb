package com.n26.api.swagger;

import com.n26.api.exception.handler.ErrorResponse;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@ApiResponses({
        @ApiResponse(
                code = 400,
                message = "bad request",
                response = ErrorResponse.class),
        @ApiResponse(
                code = 500,
                message = "internal service exception",
                response = ErrorResponse.class),
        @ApiResponse(
                code = 200,
                message = "success")
})
public @interface GroupedSwaggerApiResponses {

}
