package com.n26.api;

import com.n26.api.response.StatisticDto;
import com.n26.api.swagger.GroupedSwaggerApiResponses;
import com.n26.entity.Statistic;
import com.n26.service.StatisticService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/statistics")
public class StatisticController {

    private final StatisticService statisticService;

    @Autowired
    public StatisticController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }


    @GetMapping()
    @ApiOperation(
            value = "Gets all the transactions within 1 min.",
            nickname = "get stats ",
            response = StatisticDto.class
    )
    @GroupedSwaggerApiResponses
    public ResponseEntity<StatisticDto> findAll() {

        Statistic statistics = this.statisticService.findAll();
        StatisticDto responseStats = StatisticDto.builder()
                .build().getFromStatistics(statistics);
        return new ResponseEntity<>(responseStats
                , HttpStatus.OK);
    }

}
