package com.n26.entity;

import java.time.Instant;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Statistic {

    private Instant date;
    private Double sum;
    private Double avg;
    private Double max;
    private Double min;
    private Long count;
}
