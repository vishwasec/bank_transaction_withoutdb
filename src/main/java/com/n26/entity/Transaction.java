package com.n26.entity;

import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Transaction {
    @NotNull
    @DecimalMax("0x1.fffffffffffffP+1023")
    @DecimalMin("0x0.0000000000001P-1022")
    private Double amount;
    private Date date;

}
