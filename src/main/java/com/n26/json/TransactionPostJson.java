package com.n26.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel
public class TransactionPostJson {

    @NotNull
    @Min(0)
    private Double amount;

    @NotNull
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss.sssZ")
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date timestamp;

}
