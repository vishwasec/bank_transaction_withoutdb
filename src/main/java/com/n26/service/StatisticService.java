package com.n26.service;

import com.n26.entity.Statistic;
import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;

public interface StatisticService {

    Statistic findAll();

    void add(Transaction transaction) throws TransactionExpiredException, TransactionOutOfFutureWindow;

    void delete();
}
