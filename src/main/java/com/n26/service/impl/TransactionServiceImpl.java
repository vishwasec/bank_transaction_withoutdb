package com.n26.service.impl;

import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;
import com.n26.json.TransactionPostJson;
import com.n26.service.StatisticService;
import com.n26.service.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final StatisticService statisticService;

    @Autowired
    public TransactionServiceImpl(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @Override
    public Transaction process(TransactionPostJson json) throws TransactionExpiredException, TransactionOutOfFutureWindow {

        Transaction transaction = Transaction.builder().
                amount(json.getAmount()).
                date(json.getTimestamp()).build();

        this.statisticService.add(transaction);

        return transaction;

    }

    @Override
    public void delete() {
        statisticService.delete();
    }

}
