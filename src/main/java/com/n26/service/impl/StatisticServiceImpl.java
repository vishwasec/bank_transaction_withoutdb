package com.n26.service.impl;

import com.n26.entity.Statistic;
import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;
import com.n26.service.StatisticService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StatisticServiceImpl implements StatisticService {

    private final Object LOCK = new Object();
    private Statistic[] statistics;

    @Value("${statisticService.allowedms:60000l}")
    private Long allowedms;

    public StatisticServiceImpl() {
        initstats();
    }

    private void initstats() {
        statistics = new Statistic[10];
        for (int i = 0; i < statistics.length; i++) {
            statistics[i] = createInitStatistic(Instant.now());
        }
    }

    private Statistic createInitStatistic(Instant timestamp) {
        return Statistic.builder().date(timestamp).
                max(Double.MIN_VALUE).
                avg(0.00).
                min(Double.MAX_VALUE).
                sum(0.0).count(0L).build();
    }

    private int getTransactionIndex(Long txnTime) {

        long currTime = Instant.now().toEpochMilli();

        return (int) ((int) ((currTime - txnTime) / allowedms) % (allowedms / 1000));
    }


    @Override
    public void add(Transaction transaction) throws TransactionExpiredException, TransactionOutOfFutureWindow {
        Instant currentTimestamp = Instant.now();
        Instant transactionTimestamp = Instant.ofEpochMilli(transaction.getDate().getTime());

        checkIfvalid(currentTimestamp, transactionTimestamp);
        log.info("Processing for {} with date diff of {}", transaction.getAmount(), Duration.between(transactionTimestamp, currentTimestamp));
        synchronized (LOCK) {
            int index = getTransactionIndex(transactionTimestamp.toEpochMilli());
            Statistic statistic = statistics[index];
            if (statistic == null || statistic.getDate().plus(Duration.ofMillis(allowedms)).isBefore(currentTimestamp)) {
                statistic = this.createInitStatistic(currentTimestamp);
                statistics[index] = statistic;

            }
            if (transaction.getAmount() > statistic.getMax())
                statistic.setMax(transaction.getAmount());
            if (transaction.getAmount() < statistic.getMin())
                statistic.setMin(transaction.getAmount());

            statistic.setSum(statistic.getSum() + transaction.getAmount());
            statistic.setCount(statistic.getCount() + 1);
            statistic.setAvg(statistic.getSum() / statistic.getCount());
        }
    }

    private void checkIfvalid(Instant currentTimestamp, Instant transactionTimestamp) throws TransactionExpiredException, TransactionOutOfFutureWindow {
        if (transactionTimestamp.plus(Duration.ofMillis(allowedms)).isBefore(currentTimestamp)) {
            throw new TransactionExpiredException("Transaction has expired. Given timestamp is older than 1 min.");
        }
        if (transactionTimestamp.isAfter(currentTimestamp)) {
            throw new TransactionOutOfFutureWindow("Transaction out of window. Given timestamp is in future time.");
        }
    }

    @Override
    public void delete() {
        initstats();

    }

    @Override
    public Statistic findAll() {
        Instant currentTimestamp = Instant.now();
        Statistic resultstat = createInitStatistic(currentTimestamp);
        Arrays.stream(statistics).filter(stats -> stats.getDate().plus(Duration.ofMillis(allowedms))
                .isAfter(currentTimestamp)).forEach(stats -> {
            if (stats.getMax() > resultstat.getMax())
                resultstat.setMax(stats.getMax());
            if (stats.getMin() < resultstat.getMin())
                resultstat.setMin(stats.getMin());
            resultstat.setSum(resultstat.getSum() + stats.getSum());
            resultstat.setCount(resultstat.getCount() + stats.getCount());
            if (resultstat.getCount() == 0) {
                resultstat.setAvg(0.00);
            } else
                resultstat.setAvg(resultstat.getSum() / resultstat.getCount());
        });

        return resultstat;
    }
}