package com.n26.service;

import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;
import com.n26.json.TransactionPostJson;

public interface TransactionService {

    Transaction process(TransactionPostJson json) throws TransactionExpiredException, TransactionOutOfFutureWindow;

    void delete();
}
