package com.n26.api;

import com.n26.Application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(Application.class)
@RunWith(SpringRunner.class)
public class StatisticControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getStatisticsSuccess() throws Exception {
        this.mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk());
    }

}
