package com.n26.api;

import com.n26.Application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(Application.class)
@RunWith(SpringRunner.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void postTransactionSuccess() throws Exception {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = new Date();
        String datestring = dateformat.format(date);

        String json = "{" +
                "\"amount\": 55.0" +
                "," +
                "\"timestamp\": \"" + datestring + "\"" +
                "}";
        this.mockMvc.perform(post("/transactions")
                .contentType("application/json;charset=UTF-8")
                .content(json)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Test
    public void deleteTransactionSuccess() throws Exception {
        this.mockMvc.perform(delete("/transactions"))
                .andExpect(status().isNoContent());
    }

}
