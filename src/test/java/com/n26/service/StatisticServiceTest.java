package com.n26.service;

import com.n26.entity.Statistic;
import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StatisticServiceTest {

    @Autowired
    private StatisticService statisticService;

    @Test()
    public void addSuccess() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = new Date();
        String datestring = dateformat.format(date);

        Transaction transaction = Transaction.builder().build();
        transaction.setAmount(5.0);
        transaction.setDate(dateformat.parse(datestring));

        this.statisticService.add(transaction);
    }

    @Test(expected = TransactionExpiredException.class)
    public void addExpired() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = new Date(System.currentTimeMillis() - 3600 * 1000);
        String datestring = dateformat.format(date);

        Transaction transaction = Transaction.builder().build();
        transaction.setAmount(5.0);
        transaction.setDate(dateformat.parse(datestring));

        this.statisticService.add(transaction);
    }

    @Test(expected = TransactionOutOfFutureWindow.class)
    public void addFuture() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = new Date(System.currentTimeMillis() + 3600 * 1000);
        String datestring = dateformat.format(date);

        Transaction transaction = Transaction.builder().build();
        transaction.setAmount(5.0);
        transaction.setDate(dateformat.parse(datestring));

        this.statisticService.add(transaction);
    }

    @Test
    public void findallsuccess() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        statisticService.delete();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = new Date();
        String datestring = dateformat.format(date);

        Transaction tr1 = Transaction.builder().build();
        tr1.setAmount(5.0);
        tr1.setDate(dateformat.parse(datestring));
        this.statisticService.add(tr1);


        Transaction tr2 = Transaction.builder().build();
        tr2.setAmount(15.0);
        tr2.setDate(dateformat.parse(datestring));
        this.statisticService.add(tr2);


        Statistic statistic = this.statisticService.findAll();
        Assert.assertEquals(Long.valueOf(2l), statistic.getCount());
        Assert.assertEquals(Double.valueOf(5.0), statistic.getMin());
        Assert.assertEquals(Double.valueOf(15.0), statistic.getMax());
        Assert.assertEquals(Double.valueOf(20.0), statistic.getSum());
        Assert.assertEquals(Double.valueOf(10.0), statistic.getAvg());
    }

}
