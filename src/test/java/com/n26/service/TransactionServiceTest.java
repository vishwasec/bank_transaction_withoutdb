package com.n26.service;

import com.n26.entity.Transaction;
import com.n26.exception.TransactionExpiredException;
import com.n26.exception.TransactionOutOfFutureWindow;
import com.n26.json.TransactionPostJson;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;


    @Test(expected = TransactionExpiredException.class)
    public void processExpiredException() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = new Date(System.currentTimeMillis() - 3600 * 1000);
        String datestring = dateformat.format(date);

        TransactionPostJson json = new TransactionPostJson();
        json.setAmount(6.0);
        json.setTimestamp(dateformat.parse(datestring));

        this.transactionService.process(json);
    }

    @Test(expected = TransactionOutOfFutureWindow.class)
    public void processFutureException() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = new Date(System.currentTimeMillis() + 3600 * 1000);
        String datestring = dateformat.format(date);


        TransactionPostJson json = new TransactionPostJson();
        json.setAmount(6.0);
        json.setTimestamp(dateformat.parse(datestring));

        this.transactionService.process(json);
    }

    @Test
    public void processSuccess() throws TransactionExpiredException, TransactionOutOfFutureWindow, ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = new Date(System.currentTimeMillis());
        String datestring = dateformat.format(date);


        TransactionPostJson json = new TransactionPostJson();
        json.setAmount(6.0);
        json.setTimestamp(dateformat.parse(datestring));

        Transaction transaction = this.transactionService.process(json);

        Assert.assertEquals(json.getAmount(), transaction.getAmount());
    }

}
