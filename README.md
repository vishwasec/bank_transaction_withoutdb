**Backend Challenge**

API to record and retrieve real-time statistics abot transactions in a given timeframe.

We would like to have a restful API for our statistics. The main use case for our API is to calculate realtime statistic from the last 60 seconds. There will be two APIs, one of them is called every time a transaction is made. It is also the sole input of this rest API. The other one returns the statistic based of the transactions of the last 60 seconds.

Specs POST /transactions

Every Time a new transaction happened, this endpoint will be called. Body: { "amount": 12.3, "timestamp": "2019-02-21T01:05:53.242Z" }

Where: ● amount - transaction amount

● timestamp - transaction time in epoch in millis in UTC time zone (this is not current timestamp)

Returns: Empty body with either 201 or 204.

● 201 - in case of success

● 204 - if transaction is older than 60 seconds

Where:

● amount is a double specifying the amount

● time is a long specifying unix time format in milliseconds

GET /statistics This is the main endpoint of this task, this endpoint have to execute in constant time and memory (O(1)). It returns the statistic based on the transactions which happened in the last 60 seconds.

Returns: { "sum": 1000, "avg": 100, "max": 200, "min": 50, "count": 10 }

Where:

● sum is a double specifying the total sum of transaction value in the last 60 seconds

● avg is a double specifying the average amount of transaction value in the last 60 seconds

● max is a double specifying single highest transaction value in the last 60 seconds

● min is a double specifying single lowest transaction value in the last 60 seconds

● count is a long specifying the total number of transactions happened in the last 60 seconds

Requirements

For the rest api, the biggest and maybe hardest requirement is to make the GET /statistics execute in constant time and space. The best solution would be O(1). It is very recommended to tackle the O(1) requirement as the last thing to do as it is not the only thing which will be rated in the code challenge.

Other requirements, which are obvious, but also listed here explicitly:

● The API have to be threadsafe with concurrent requests

● The API have to function properly, with proper result

● The project should be buildable, and tests should also complete successfully. e.g. If maven is used, then mvn clean install should complete successfully.

● The API should be able to deal with time discrepancy, which means, at any point of time, we could receive a transaction which have a timestamp of the past

● Make sure to send the case in memory solution without database (including in-memory database)

● Endpoints have to execute in constant time and memory (O(1))


---

## How to run

1. Install Maven locally
2. Run: mvn spring-boot:run

## Saves transaction.
Save the transaction in constant time.

Modify the timestamp to current utc time.

curl -X POST \
  http://localhost:8080/transactions \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json;charset=UTF-8' \
  -H 'Postman-Token: 602050e1-9c2d-4c25-bcd4-0e575315a477' \
  -d '{  
   "amount": 50,  
   "timestamp": "2019-02-21T01:05:53.242Z"  
 }'
 
## Gets all active transactions within 1 min.
curl -X GET \
  http://localhost:8080/statistics \
  -H 'Accept: application/json' \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 3de6b045-6dfb-4f51-974e-28539d611ce3'
  
## Delete tx.
To remove all the transactions.

curl -X DELETE --header 'Accept: */*' 'http://localhost:8080/transactions'


## To know more run the app with the above command and check.
http://localhost:8080/swagger-ui.html

